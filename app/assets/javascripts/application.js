//= require jquery
//= require bootstrap
//= require dataTables
//= require qtip

$(document).ready(function () {
    $('#build-info-table').DataTable({
            "oLanguage": {"sSearch": ""},
            "order": [[ 4, "desc" ]]
        }
    );
    $('.dataTables_filter input').attr("placeholder", "Search");
    $('.dataTables_filter input').attr("class", "form-control");
    $('[title!=""]').qtip();
    $('.hide-jumbotron').click(function () {
        $('.jumbotron').slideUp();
    });
});