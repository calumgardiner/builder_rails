class UserController < ApplicationController

  def show
    @user = User.find_by_user_name(params[:id])
  end

  def index
    @users = User.all
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      flash[:success] = 'Welcome to builder!'
      redirect_to root_url
    else
      render 'new'
    end
  end

  private
  def user_params
    params.require(:user).permit(:user_name, :user_email, :password, :password_confirmation)
  end

end