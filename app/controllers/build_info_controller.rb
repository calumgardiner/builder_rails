class BuildInfoController < ApplicationController

  # Show a build based on the id passed to the url
  def show
    @build_info = find
    attach_steps @build_info
    attach_guide @build_info
  end

  # Find a build based on the id passed to the url
  # if user is logged in checks what their vote was
  # on this build and attaches it to the object
  def find
    @build_info = BuildInfo.find(params[:id])
    if (!@build_info.nil? && requires_update?(@build_info))
      update_votes(@build_info)
    end
    if logged_in?
      @build_info.user_vote = Vote.where('build_info_id = ? AND user_id = ?', @build_info.id, current_user.id).first
    end
    @build_info
  end

  # Get all builds
  def index
    @build_infos = BuildInfo.all
  end

  # Upvote the build for the id passed to the url
  def upvote
    # ensure user logged in
    if logged_in?
      @build_info = find
      # If current vote is upvote then clear
      if (!@build_info.user_vote.nil? && @build_info.user_vote.upvote)
        @build_info.user_vote.upvote = false
        # Else true upvote and false downvote
      else
        @build_info.user_vote = Vote.new if @build_info.user_vote.nil?
        @build_info.user_vote.user_id = current_user.id
        @build_info.user_vote.build_info_id = @build_info.id
        @build_info.user_vote.upvote = true
        @build_info.user_vote.downvote = false
      end
      @build_info.user_vote.save
      redirect_to build_info_path @build_info
    else
      redirect_to signup_url
    end
  end

  # Downvote the build for the id passed to the url
  def downvote
    # ensure user logged in
    if logged_in?
      @build_info = find
      # If current vote is downvote then clear
      if (!@build_info.user_vote.nil? && @build_info.user_vote.downvote)
        @build_info.user_vote.downvote = false
        # Else true downvote and false upvote
      else
        @build_info.user_vote = Vote.new if @build_info.user_vote.nil?
        @build_info.user_vote.user_id = current_user.id
        @build_info.user_vote.build_info_id = @build_info.id
        @build_info.user_vote.upvote = false
        @build_info.user_vote.downvote = true
      end
      @build_info.user_vote.save
      redirect_to build_info_path @build_info
    else
      redirect_to signup_url
    end
  end

  private
  # Require update if never updated or updated longer than 5 minutes ago
  def requires_update?(build_info)
    (build_info.updated.nil? or build_info.updated < 5.minutes.ago)
  end

  # Search for votes relating to a build_info
  # Set them in the object and SAVE
  def update_votes(build_info)
    upvotes = 0
    downvotes = 0
    Vote.where(:build_info_id => build_info.id.to_i).all.each do |vote|
      upvotes += 1 if vote.upvote
      downvotes += 1 if vote.downvote
    end
    build_info.update(upvotes: upvotes)
    build_info.update(downvotes: downvotes)
    build_info.update(updated: Time.now)
    build_info.save
  end

  def attach_steps(build_info)
    build_info.build_steps = Step.where('build_info_id = ?', build_info.id).order('ordering')
    build_info
  end

  def attach_guide(build_info)
    build_info.build_guide = Guide.where('build_info_id = ?', build_info.id).first
    build_info
  end

end