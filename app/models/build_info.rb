class BuildInfo < ActiveRecord::Base

  attr_accessor :user_vote
  attr_accessor :build_steps
  attr_accessor :build_guide

  belongs_to :build_type
  belongs_to :race
  belongs_to :versus_race, :foreign_key => 'versus_race_id', :class_name => "Race"
  belongs_to :difficulty
  belongs_to :expansion
  belongs_to :user

  has_many :key_units
  has_many :units, :through => :key_units

  def to_param
    "#{id}-#{slug}"
  end

  def slug
    name.downcase.gsub(' ', '-')
  end

  def rating
    upvotes-downvotes
  end

  def update_votes?
    requires_update(self)
  end

  def update_voting
    update_votes(self)
  end

end