class User < ActiveRecord::Base
  attr_accessor :remember_token

  has_many :build_infos

  before_save { self.user_email = user_email.downcase }
  validates_presence_of :user_name
  validates_uniqueness_of :user_name, :case_sensitive => false
  validates_uniqueness_of :user_email
  validates :password, length: {minimum: 8}
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :user_email, presence: true, length: {maximum: 255}, format: {with: VALID_EMAIL_REGEX}
  has_secure_password

  def to_param
    user_name.downcase
  end

  # Returns the hash digest of the given string.
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
        BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  # Returns a random token.
  def User.new_token
    SecureRandom.urlsafe_base64
  end

  # Remembers a user in the database for use in persistent sessions.
  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end

  def authenticated?(remember_token)
    return false if remember_digest.nil?
    BCrypt::Password.new(remember_digest).is_password?(remember_token)
  end

  # Forgets a user.
  def forget
    update_attribute(:remember_digest, nil)
  end

end